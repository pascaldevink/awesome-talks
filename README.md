# Architecture and design
- Ross Tuck - Everything is Anything: The Unlikely Wisdom of Historical Stabbings (https://www.youtube.com/watch?v=GObX6DJ7afM)
- Sandi Metz - All the little things (https://www.youtube.com/watch?v=8bZh5LMaSmE)
- Sandi Metz - Nothing is something (https://www.youtube.com/watch?v=9lv2lBq6x4A)
- Rebecca Wirfs-Brock - Why We Need Architects (and Architecture) on Agile Project (https://www.youtube.com/watch?v=YPZR0-_Uh0o)
- Paul Rayner — How Agile Can Cripple Effective Design Work (and what to do about it) (https://www.youtube.com/watch?v=Bl30X0MvT0I)
- Simon Brown - Software Architecture vs. Code (https://www.youtube.com/watch?v=GAFZcYlO5S0)
# Devops

# Languages and development

- Rafael Dohms - Your code sucks, let's fix it (https://www.youtube.com/watch?v=GtB5DAfOWMQ)
- Uncle Bob - Functional Programming; What? Why? When? (https://www.youtube.com/watch?v=7Zlp9rKHGD4)

## PHP

## Ruby

## Scala

## Java

## Javascript

# Enjoyable
- Brandon Hays - Hacking Spacetime for a Successful Career (https://youtu.be/TrLDU6u_-rY)
- Ross Tuck - Things I Believe Now That I'm Old (https://www.youtube.com/watch?v=ZBiexI2mv-k)
- Keith Ferrazzi - How to Create Highly Effective and High Performing Teams (https://www.youtube.com/watch?v=k_hQrMbQdA0)